<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'prefix' => 'v1',
        'namespace' => 'App\Http\Controllers\Api'
    ],
    function() {
        Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
            Route::post('register', 'UsersApiController@store');
            Route::post('login', 'UsersApiController@login');
            // Route::post('register', function(){
            //     return "hello";
            // });
        });

        Route::group(
            [
                'middleware' => 'auth:api',
            ],
            function () {
                Route::apiResource('customer/measurement', 'Customer\CustomerMeasurementsController');
                Route::apiResource('customer', 'Customer\CustomersController');
            });
    }
);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
