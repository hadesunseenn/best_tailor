<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Customer\MeasurementResource;
use App\Models\Customer\Measurement;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class CustomerMeasurementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $customer_id = $request->customer_id;

        $measurements = Measurement::where('user_id', $user->id)
            ->where('customer_id', $customer_id)
            ->latest()
            ->get();

        return (MeasurementResource::collection($measurements))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $validatedData = $request->validate([
            'image' => 'required|mimes:jpeg,jpg,png|max:3000'
        ]);
        
        $file = $request->file('image');
   
        $image_name= str_replace(' ', '_', $file->getClientOriginalName());
        
        $image_name = Uuid::uuid().'-'.$image_name;
        $image_name = 'measurements/'.$image_name;
        $file = Storage::disk('s3')->put($image_name, file_get_contents($file));

        // $file = Storage::disk('s3')->temporaryUrl($image_name, now()->addSeconds('3600'));
        // print_r($request->all());
        $data['user_id'] = $user->id;
        $data['customer_id'] = $request->input('customer_id');
        $data['image'] = $image_name;
        $data['notes'] = $request->input('notes');
        $measurement = Measurement::create($data);

        return (new MeasurementResource($measurement))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
