<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class MeasurementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'image' => Storage::disk('s3')->temporaryUrl($this->image, now()->addSeconds('3600')),
            'created_at' => $this->created_at,
            'notes' => $this->notes
        ];
    }
}
