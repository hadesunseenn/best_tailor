<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    use HasFactory;

    protected $table = "customer_measurements";

    protected $fillable = [
        'user_id',
        'customer_id',
        'image',
        'notes'
    ];
}
