import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store/store'
import './plugins/base'
import config from './config';
import axios from 'axios';
import moment from 'moment';


Vue.config.productionTip = false;
axios.defaults.baseURL = config.SERVER_API_URL;

const token = localStorage.getItem('token');

if (token) {
    axios.defaults.headers.common['Authorization'] = "Bearer " + token;
}

Vue.filter('formatDate', function (value) {
    if (value) {
      return moment(String(value)).format('DD-MM-YYYY h:m:s a')
    }
  });

new Vue({
    vuetify,
    store,
    router,
    render: h => h(App)
}).$mount('#app')

// Axios Middleware
axios.interceptors.response.use(
    res => res,
    err => {
      if (err.response.status === 404) {
        throw new Error(`${err.config.url} not found`);
      }
  
      if (err.response.status == 401 &&
        err.response.data &&
        err.response.data.errors &&
        err.response.data.errors.message == 'logout') {
        store.dispatch('logout').then(() => {
          router.push('/login')
        }).catch(err => { throw err });
      }
      throw err;
    });