import validationErrors from '../../shared/components/ValidationErrors';
export default {
  data() {
    return {
      validationErrors: []
    }
  },
  components: {
    validationErrors
  },
  methods: {
    formatErrors(items) {
      let errors = [];

      (items || []).map(value => {
        errors.push(value[0]);
      });

      return errors;
    },
    processCatch(error) {
      let items;
      if (error.response.status == 500) {
        return [
          "Unable to process request. Please try again later"
        ];
      } else {
        if (error.response.status == 400) {
          items = Object.values(error.response.data.data.message);
        } else if (error.response.status == 422) {
          items = Object.values(error.response.data.errors);
        }
        return this.formatErrors(items);
      }
    }
  }
};
