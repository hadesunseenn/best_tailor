import Vue from "vue";
import axios from 'axios';

const state = {
  token: null,
  expiration: null,
  resent: false,
  valid: null,
};

const getters = {
  getUser: state => {
    return state.user;
  },
  isAuthenticated: state => {
    return state.token !== null;
  },
  isResent: state => {
    return state.resent;
  },
  isValidated: state => {
    return state.valid;
  },
  token: state => state.token,
};


const mutations = {
  setToken(state, token) {
    state.token = token;
  },
  // setExpiration(state, expiration) {
  //   state.expiration = expiration;
  // },
  // setUser(state, user) {
  //   state.user = user;
  // },
  logout(state) {
    state.token = null;
    state.user = null;
  },
  setRegisterReturnData(state, data) {
    localStorage.setItem('token', data.token);
    localStorage.setItem("user_id", data.id);
    state.token = data.token;
    state.user = data;
  }
};


const actions = {
  login({ commit, dispatch }, user) {
    return new Promise((resolve, reject) => {
      try {
        axios.post('v1/auth/login', user)
        .then((response) => {
          let res = response.data.data;
          const token = res.token;
          axios.defaults.headers.common['Authorization'] = "Bearer " + token;

          commit('setRegisterReturnData', res);
          resolve(res)
        })
        .catch((error) => {
          commit('setToken', null);
          // commit('setUser', null);
          // commit('setExpiration', null);
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          reject(error);  
        })
        // dispatch('fetchSettings').then(() => {
        //   resolve(res);
        // });
      } catch (error) {
        commit('setToken', null);
        commit('setUser', null);
        commit('setExpiration', null);
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        reject(error);
      }
      
    });
  },

  logout({commit}) {
    return new Promise((resolve, reject) => {
      commit("logout");
      window.localStorage.clear();
      localStorage.removeItem("vuex");
      localStorage.removeItem("user_id");
      localStorage.removeItem("token");
      delete axios.defaults.headers.common['Authorization'];
      resolve();
    });
  }
}

// Export the information
export default {
  state,
  getters,
  mutations,
  actions
};