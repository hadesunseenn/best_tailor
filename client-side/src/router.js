import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: 'login',
    },
    {
      path: '/login',
      component: () => import('@/layouts/AuthLayout'),
      children: [
        {
          name: 'Login',
          path: '',
          component: () => import('@/views/auth/Login'),
        },
        {
          name: 'Logout',
          path: '/',
          component: () => import('@/views/auth/Logout'),
        }

      ]
    },
    {
      path: '/dashboard',
      component: () => import('@/layouts/Layout'),
      children: [

        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        {
          name: 'Customers',
          path: 'customers',
          component: () => import('@/views/dashboard/Customers'),
        },
        {
          name: 'Customer Details',
          path: 'customer-details/:customer_id',
          component: () => import('@/views/dashboard/CustomerDetails'),
        },

      ]
    },

  ],
})